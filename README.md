# mati-arqsw-flask-api-mngr

## Software Architecture - Challenge 2 (Availability and Security)

The `mati-arqsw-flask-api-mngr` is designed leveraging the principles of Hexagonal Architecture to ensure a clean separation of concerns, making it more adaptable to changes and easier to test. This architecture divides the application into several layers, each with its own responsibility.

- **Adapters Layer**: Located within the `adapters` directory, this layer contains the controllers and presentation logic. It acts as the bridge between the external world and our application, converting external requests into a format that the application can understand.

- **Application Layer**: This is the heart of our business logic. Here, we define the services that execute operations related to our business rules. It's crafted to be independent of any external interfaces, ensuring that core functionality can evolve independently of external changes.

- **Infrastructure / Server Layer**: Found in the `infrastructure` directory, this layer manages the technical details and configurations necessary for running our application. It includes server configurations and external integrations, abstracting the complexities of external communications from the core logic of the application.

- **Domain Layer**: The domain layer hosts our data interfaces and repositories. Located in the `domain` directory, it defines the business entities and the contracts for data access, ensuring that the application can interact with data sources in a consistent manner.

- **Bin Layer**: The initialization of the server is handled in the `bin` layer. It's responsible for bootstrapping the application, setting up the necessary configurations for the application to run.

For integration and continuous deployment, the API utilizes a `config.ts` file for environment variables, facilitating seamless integration with CI/CD pipelines. This approach not only enhances the maintainability and scalability of the application but also streamlines the deployment process, ensuring that the application can be easily integrated into various environments with minimal configuration changes.

In summary, the `mati-arqsw-flask-api-mngr` is structured to promote flexibility, maintainability, and scalability, adhering to best practices in software architecture. It's an ideal starting point for building flask. APIs that require a robust structure for long-term evolution and integration.

## Repository Purpose

The primary aim of this repository is to maintain the flask API Manager template, designed with a focus on strategically enhancing and managing key Architectural Significant Requirements (ASRs) such as:

- **Availability**: Ensuring that the system is reliable, resilient, and capable of handling requests without significant downtime. The template incorporates patterns and practices aimed at maximizing uptime and ensuring that services are always accessible when needed.

- **Security**: Placing a high priority on safeguarding the system against unauthorized access and potential security threats. The template is crafted to include best practices in security, from authentication and authorization to data encryption and secure communication protocols.

By leveraging this template, developers can create robust and secure flask APIs that are well-equipped to handle critical business tactics, such as voting mechanisms, with efficiency and reliability. It serves as a solid foundation for building applications that require high levels of availability and security, streamlining the development process and ensuring adherence to industry standards.

## Installation

## Installation Instructions and Local Execution

- Make sure you have at least Python 3.8 installed. Check the version with the command: `$ python --version`

- Create and activate the virtual environment:

```sh
python3 -m venv venv
source ./venv/bin/activate
```

- Install the development dependencies:

```sh
pip3 install -r requirements/dev.txt
pip3 install boto3 # should not be part of the requirements.txt
```

- In file `app/infrastructure/server.py` add line

```py
APP.run(debug=True)
```

- Run the app locally

```sh
python3 index.py
```

For development, activate the virtual environment and set the environment variable for Flask:

```sh
source ./venv/bin/activate
export FLASK_APP=server.py
```

Start the development server:

```sh
flask run
```

## Get the list of dependencies if necessary

Update and save the current dependencies of python like so:

```sh
pip3 freeze > requirements.txt
```

# Testing execution

To run the tests, follow the steps below:

- Activate the virtual environment:

```sh
source ./venv/bin/activate
```

- Run all the tests:

```sh
pytest -s
```

- Run a specific test:

```sh
pytest test/test_secretController.py
```

- Run a specific test showing logs:

```sh
pytest test/test_secretController.py -s
```

## Format files

- Activate the virtual environment:

```sh
source ./venv/bin/activate
```

- Install the formatting tools:

```sh
pip3 install autopep8
pip3 install autoflake
```

- Format files automatically:

```sh
python3 -m autopep8 --in-place --recursive *.py
python3 -m autopep8 --in-place --recursive app
```

- Delete unused imports:

```sh
autoflake --in-place --remove-all-unused-imports app/**/*.py test/**/*.py
```

## Making API Requests

Currently, the API provides a single endpoint that supports the POST HTTP method. To interact with this endpoint, you can use the following cURL command:

```cURL
curl --location 'http://localhost:5000/v1/product/tax' \
--header 'X-RqUID: 12345' \
--header 'X-Name: API node js challenge 2' \
--header 'Content-Type: application/json' \
--data '{
    "id": "string",
    "amount": 100000,
    "idClient": "string",
    "email": "string"
}'
```

## members

- Joann Castellanos
- Nicolás Silva
- Andrés Guerrero
- Andrés Felipe Wilches Torres
