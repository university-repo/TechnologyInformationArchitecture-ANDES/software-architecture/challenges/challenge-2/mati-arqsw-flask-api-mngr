CURRENT_DIRECTORY=$(pwd)
cd ${pythonLocation}/lib/python3.8/site-packages
DEPENDENCIES_LIST=$(ls -1)
cd $CURRENT_DIRECTORY
cp -r ${pythonLocation}/lib/python3.8/site-packages/* .
echo "DEPENDENCIES_LIST => " ${DEPENDENCIES_LIST}
echo '::set-output name=DEPENDENCIES_LIST::'${DEPENDENCIES_LIST}