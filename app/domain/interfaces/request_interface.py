from pydantic import BaseModel, Field
from typing import List, Optional

class RequestData(BaseModel):
    id: str
    amount: int
    idClient: str
    email: str

class RequestHeaders(BaseModel):
    X_Name: str = Field(..., alias='X_Name')
    X_RqUID: str = Field(..., alias='X_Rquid')
    Content_Type: str = Field(..., alias='Content_Type')
