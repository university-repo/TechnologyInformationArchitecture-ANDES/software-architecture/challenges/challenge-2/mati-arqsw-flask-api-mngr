import logging

def configure_logger(name):
    """Configure logging with file path"""
    root = logging.getLogger(name)
    root.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(levelname)s:arqsw:%(module)s: %(message)s')
    default_handler = logging.StreamHandler()
    default_handler.setFormatter(formatter)
    root.addHandler(default_handler)
    root.propagate = False
    return root
