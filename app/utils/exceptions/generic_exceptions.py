KEY_EMPTY = 'Key Empty'
EXCEPTION_MESSAGE = 'Exception: {}'


class GeneralException(Exception):

    def __init__(self, message: str = KEY_EMPTY):
        super().__init__(message)
        self.message = message
        self.status_code = 500
        self.status = 'INTERNAL_SERVER_ERROR'

    def __str__(self) -> str:
        return EXCEPTION_MESSAGE.format(self.message)

    def to_dict(self) -> dict:
        return {"error": str(self)}


class NotAuthorizedException(Exception):

    def __init__(self, message: str = KEY_EMPTY):
        super().__init__(message)
        self.message = message
        self.status_code = 401
        self.status = 'NOT_AUTHORIZED'

    def __str__(self) -> str:
        return EXCEPTION_MESSAGE.format(self.message)

    def to_dict(self) -> dict:
        return {"error": str(self)}


class NotFoundException(Exception):

    def __init__(self, message: str = 'Key Empty'):
        super().__init__(message)
        self.message = message
        self.status_code = 404
        self.status = 'NOT_FOUND'

    def __str__(self) -> str:
        return EXCEPTION_MESSAGE.format(self.message)

    def to_dict(self) -> dict:
        return {"error": str(self)}


class GetRepoGitHubNoExistsError(Exception):
    pass


class GetTeamGitHubNoExistsError(Exception):
    pass


class ApiKeyNotFoundInParameterStore(Exception):

    def __init__(self, message=KEY_EMPTY):
        super().__init__(message)
        self.message = message
        self.status_code = 404
        self.status = 'NOT_FOUND'

    def __str__(self) -> str:
        return EXCEPTION_MESSAGE.format(self.message)
