from flask import request
import uuid

def get_request_uuid(headers):
    """
    Retrieves the UUID associated with a request, generating a new one if none is present.

    If the 'X-RqUID' header is set and not empty, this value is returned as the UUID for the request.
    If the 'X-RqUID' header is not set or empty, a new UUID is generated and returned.

    :return: The UUID associated with the request.
    """
    rq_uuid = headers.X_RqUID
    if not rq_uuid:
        rq_uuid = str(uuid.uuid4())

    return rq_uuid
