from flask import jsonify

def success_response(data, status=200):
    """
    Generates a success HTTP response.

    :param data: The data to be included in the response body.
    :param status: The HTTP status code (default is 200).
    :return: A Flask Response object.
    """
    return jsonify(data), status

def error_response(message, status):
    """
    Generates an error HTTP response.

    :param message: The error message to be included in the response body.
    :param status: The HTTP status code.
    :return: A Flask Response object.
    """
    error_data = {'error': message, 'statusCode': status}
    return jsonify(error_data)
