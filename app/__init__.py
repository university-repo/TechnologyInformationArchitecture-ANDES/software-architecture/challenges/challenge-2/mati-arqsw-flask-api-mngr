from flask import Flask
import os

from app.adapters.routes.tax_route import TaxRoute

def create_app():
    app = Flask(__name__, instance_relative_config=True)  # NOSONAR
    relative_path = "./app"
    absolute_path = os.path.abspath(relative_path)
    assets_path = os.path.join(absolute_path,"../assets/")

    TaxRoute(app)

    return app

if __name__ == "__main__":
    APP = create_app()
    APP.run(debug=True)
