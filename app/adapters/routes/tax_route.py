from flask import Blueprint
from app.adapters.controllers.tax_controller import TaxController

class TaxRoute:
    def __init__(self, app):
        self.app = app
        self.tax_router = Blueprint('flask_route', __name__)
        self.register_routes()

    def register_routes(self):
        @self.tax_router.route('/tax', methods=['POST'])
        def get_tax():
            return TaxController.get_tax()

        self.app.register_blueprint(self.tax_router, url_prefix="/v1/product")
