from flask import request
from flask_restful import Resource
from app.services.example_service import ExampleService
from app.schemas.example_schema import ExampleSchema
from app.utils.response import success_response, error_response


class ExampleController(Resource):
    def __init__(self):
        self.example_service = ExampleService()
        self.example_schema = ExampleSchema()

    def get(self, id=None):
        if id:
            example = self.example_service.get_example_by_id(id)
            if example:
                return success_response(self.example_schema.dump(example))
            else:
                return error_response('not found', 404)
        else:
            examples = self.example_service.get_all_examples()
            return success_response(self.example_schema.dump(examples, many=True))

    def post(self):
        data = request.get_json()
        errors = self.example_schema.validate(data)
        if errors:
            return error_response(errors, 400)
        example = self.example_service.create_example(data)
        return success_response(self.example_schema.dump(example), 201)

    def put(self, id):
        data = request.get_json()
        errors = self.example_schema.validate(data)
        if errors:
            return error_response(errors, 400)
        example = self.example_service.update_example(id, data)
        if example:
            return success_response(self.example_schema.dump(example))
        else:
            return error_response('Example not found', 404)

    def delete(self, id):
        if self.example_service.delete_example(id):
            return success_response(None, 204)
        else:
            return error_response('Example not found', 404)
