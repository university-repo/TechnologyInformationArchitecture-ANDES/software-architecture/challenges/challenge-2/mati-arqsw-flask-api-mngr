import json

from app.utils import logging_utils
from app.utils.exceptions.generic_exceptions import (GeneralException,
                                                     NotFoundException)
from flask import Blueprint, Response
from uuid import uuid4


DISPATCHER = Blueprint('dispatcher', __name__)
LOGGER = logging_utils.configure_logger(__name__)

ERROR_LOGGER_MESSAGE = 'Error: %s'
APPLICATION_JSON = 'application/json'
START_REQUEST_FORMATTER_LOG = '%s %s CLIENT with UUID: %s. Request: %s'
RESPONSE_FORMATTER_LOG = '%s %s CLIENT with UUID: %s. Response: %s'
ERROR_FORMATTER_LOG = '%s %s CLIENT with UUID: %s. Error: %s'


@DISPATCHER.errorhandler(NotFoundException)
def handle_not_found_exception(error) -> Response:
    uuid = uuid4
    LOGGER.error(ERROR_LOGGER_MESSAGE, error.args, uuid)
    status_code = 404
    response = Response(json.dumps(error.to_dict()), status=status_code,
                        mimetype=APPLICATION_JSON)
    return response


@DISPATCHER.errorhandler(GeneralException)
def handle_general_exception(error) -> Response:
    uuid = uuid4
    LOGGER.error(ERROR_LOGGER_MESSAGE, error.args, uuid)
    status_code = 500
    response = Response(json.dumps(error.to_dict()), status=status_code,
                        mimetype=APPLICATION_JSON)
    return response


@DISPATCHER.errorhandler(Exception)
def handle_exception(error) -> Response:
    uuid = uuid4
    LOGGER.error(ERROR_LOGGER_MESSAGE, error.args, uuid)
    error = GeneralException(error.args)
    status_code = 500
    response = Response(json.dumps(error.to_dict()), status=status_code,
                        mimetype=APPLICATION_JSON)
    return response
