from flask import request
from pydantic import ValidationError

from app.domain.interfaces.request_interface import RequestData, RequestHeaders
from app.application.use_cases.tax_service import TaxService
from app.utils.exceptions.generic_exceptions import NotFoundException, GeneralException
from app.utils.helpers.get_uuid_utils import get_request_uuid
from app.utils.logging_utils import configure_logger
from app.utils.response import success_response, error_response
class TaxController:
    @staticmethod
    def get_tax():
        logger = configure_logger(__name__)
        try:
            print(request.get_json())
            data = RequestData.model_validate(request.get_json())
            header_data_dict = { header.replace('-','_'): value for header, value in request.headers.items() }
            headers_data = RequestHeaders.model_validate(header_data_dict)
            uuid: str = get_request_uuid(headers_data)
            print(data)
            transaction_service = TaxService({
                'uuid': uuid,
                'data': data
            })

            data = transaction_service.calculate_tax()
            return success_response(data, 200)
        except ValidationError as e:
            logger.error(f"Validation error in save_transaction: {e}")
            return error_response('Invalid Request Data', 400)
        except NotFoundException as e:
            logger.error(f"NotFoundException in save_transaction: {e}")
            return error_response('Not found', 404)
        except GeneralException as e:
            logger.error(f"GeneralException in save_transaction: {e}")
            return error_response('Internal server error', 500)
        except Exception as e:
            logger.error(f"Unhandled exception in save_transaction: {e}")
            return error_response('Internal server error', 500)
