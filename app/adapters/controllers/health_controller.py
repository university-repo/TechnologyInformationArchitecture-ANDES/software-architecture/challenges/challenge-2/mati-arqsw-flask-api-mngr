from flask import Blueprint, Response

from app.utils import logging_utils

content = 'application/json'
LOGGER = logging_utils.configure_logger(__name__)

PROJECT_BLUEPRINT = Blueprint('project', __name__)


@PROJECT_BLUEPRINT.route('/management/health', methods=['GET'])
def health() -> Response:
    LOGGER.disabled = True
    return Response("", status=204)
