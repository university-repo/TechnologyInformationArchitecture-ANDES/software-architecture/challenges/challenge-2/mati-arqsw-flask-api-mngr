import os

from app.utils import logging_utils

LOGGER = logging_utils.configure_logger(__name__)

environment = os.getenv("ENVIRONMENT") or "INTEGRATION"

class AzureFunctionsConfig:
    API_GENERATIVE_AI_KEY = os.getenv('API_GENERATIVE_AI_KEY', '')
    API_GENERATIVE_AI = os.getenv('API_GENERATIVE_AI', '')

class EmbeddingsModelConfig:
    EMBEDDINGS_ACCESS_KEY_ID = os.getenv('EMBEDDINGS_ACCESS_KEY_ID', '')
    EMBEDDINGS_KEY = os.getenv('EMBEDDINGS_KEY', '')
    EMBEDDINGS_LENGTH = int(os.getenv('EMBEDDINGS_LENGTH', 256))
    EMBEDDINGS_MODEL_AWS = os.getenv('EMBEDDINGS_MODEL_AWS', '')
    EMBEDDINGS_MODEL = os.getenv('EMBEDDINGS_MODEL', '')
    EMBEDDINGS_REGION = os.getenv('EMBEDDINGS_REGION', '')
    EMBEDDINGS_SECRET_ACCESS_KEY = os.getenv('EMBEDDINGS_SECRET_ACCESS_KEY', '')
    OPEN_SEARCH_DOMAIN = os.getenv('OPEN_SEARCH_DOMAIN', 'https://localhost:9200')

class GeminiModelConfig:
    GEMINI_KEY = os.getenv('GEMINI_KEY', '')
    GEMINI_MODEL = os.getenv('GEMINI_MODEL', '')

class GptModelConfig:
    AZURE_BALANCER_KEY = os.getenv('AZURE_BALANCER_KEY', '')
    AZURE_BALANCER_URL = os.getenv('AZURE_BALANCER_URL', '')
    AZURE_DEPLOYMENT = os.getenv('AZURE_DEPLOYMENT', '')
    AZURE_KEY = os.getenv('AZURE_KEY', '')
    AZURE_OPENAI_VERSION = os.getenv('AZURE_OPENAI_VERSION', '')
    AZURE_OPENAI_SERVICE = os.getenv('AZURE_OPENAI_SERVICE', '')
    AZURE_GPT35_DEPLOYMENT = os.getenv('AZURE_GPT35_DEPLOYMENT', '')

class BasicConfig:
    NODE_ENV = os.getenv('NODE_ENV', 'production')
    API_PATH = '/v1/product/'
    DEBUG = 'arqsw:*'
    PORT = '9080'
    AI_MODEL = os.getenv('AI_MODEL', 'GeminiModel')
    MAX_SOURCES = int(os.getenv('MAX_SOURCES', 3))
    PROJECTS_PATH = os.getenv('PROJECT_PATH', '../../../../assets/projects/')
    SEARCH_KEY = os.getenv('SEARCH_KEY', '')
    SEARCH_NAME = os.getenv('SEARCH_NAME', '')
    STORAGE_KEY = os.getenv('STORAGE_KEY', '')
    STORAGE_NAME = os.getenv('STORAGE_NAME', '')

class Config:
    basic = BasicConfig
    azureFunctions = AzureFunctionsConfig
    embeddingsConfig = EmbeddingsModelConfig
    geminiConfig = GeminiModelConfig
    gptConfig = GptModelConfig
