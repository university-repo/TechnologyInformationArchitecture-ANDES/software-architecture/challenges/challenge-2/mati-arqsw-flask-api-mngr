"""Main server of the application"""

from flask import Flask

from app.config import config
from app.config.config import Config
from app.adapters.controllers import PROJECT_BLUEPRINT

from app.utils import logging_utils

APP = Flask(__name__, instance_relative_config=True)
APP.register_blueprint(PROJECT_BLUEPRINT)
LOGGER = logging_utils.configure_logger(__name__)
LOGGER.info("Starting WSGI in environment %s", config.environment)

@APP.after_request
def after_request(response):
    """Add headers to response after finallice a service"""
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers',
                         'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type,' +
                         'Accept, Access-Control-Allow-Request-Method,*')
    response.headers.add('Access-Control-Allow-Methods',
                         'GET, POST, OPTIONS, PUT, DELETE, PATCH')
    response.headers.add('Allow', 'GET, POST, OPTIONS, PUT, DELETE, PATCH')
    return response


if __name__ == '__main__':
    LOGGER.info("Starting WSGI Server: ", config.environment)
    if config.environment == 'INTEGRATION':
        LOGGER.info("Starting WSGI development Server")
        APP.run(host='127.0.0.1', port=Config.basic.PORT, debug=True)
    else:
        LOGGER.info("Starting BJOERN Production Server")

APP.run(debug=True)