echo "" > output-list.txt
input="input-list.txt"
while IFS= read -r line
do
  curl -L -X GET "https://8dazx7fj2g.execute-api.us-east-1.amazonaws.com/laboratory/mati-arqsw-api-flask/project/$line" | jq -c | sed 's#"TeamPrefix"#\n"TeamPrefix"#g' | sed 's#"Topics"#\n"Topics"#g' | sed 's#"Description"#\n"Description"#g' >> output-list.txt
done < "$input"
