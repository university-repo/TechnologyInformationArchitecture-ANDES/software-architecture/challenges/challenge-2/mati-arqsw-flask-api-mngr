from app import serverless_wsgi
from app.infrastructure.server import APP

def handler(event, context, uuid: str):
    print("event", event)
    print("context", context)
    event["requestContext"] = {}  # mock for aws lambda
    return serverless_wsgi.handle_request(APP, event, context, uuid)
